step = 0
console.debug('%s meteor hello', step++);

// I M P O R T S
import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import { Session } from 'meteor/session';
import { Accounts } from 'meteor/accounts-base';
import { Random } from 'meteor/random';

import './main.html';

// DEFAULTS / GLOBALS VARIABLES
Accounts.ui.config({
  passwordSignupFields: 'USERNAME_AND_EMAIL',
});

const LANDING_PAGE = 'home';

// timers 
interval = {}; 
timer = {};
clock = {};

// application defaults
const True = true;
const False = false;

app = {}; 
app.settings = {'timeout':30,'back':False,'next':True,'multi':False,'auto':False,'topic':True,'min':0,'max':1, 'rank':False };
app.question = {
    "topic": "General"
  , "question": "HTML document fragment"
  , "title": "Question Title"
  , "answers":[{ 'option': 'Option one', 'points': 1 }
             , { 'option': 'Choice two', 'points': 0 }
             , { 'option': 'Last choice', 'points': 0 }]
  , "settings": app.settings
}
app.exam = {
  "id": "CS101",
  "name": "New exam",
  "field": "Quality Control",
  "module": "Unit Testing",
  "course": "Testing for Dummies",
  "level": "Intermediate",
  "award": "",
  "type": "Quiz",
  "stars": 1,
  "public": True,
  "questions": [ app.question ], 
  "timed": True
}

my = {};

// COLLECTIONS
Meteor.subscribe('users');

// templates
Exams = new Meteor.Collection("exams");
Meteor.subscribe('exams');

// one per user
Tests = new Meteor.Collection("tests");
Meteor.subscribe('tests');

// one set per test
Results = new Meteor.Collection("results");
Meteor.subscribe('results');

// select 
Lists = new Meteor.Collection('lists'); 
Meteor.subscribe('lists');

// aggregated results
Marks = new Meteor.Collection(null);

// GLOBAL FUNCTIONS
Number.prototype.clamp = function(min, max) {
  return Math.min(Math.max(this, min), max);
};

sum = (items, prop) => {
 return items.reduce( function(a, b){ return a + (parseInt(b[prop]) || 0)}, 0);
};

resizeImage = (url, width, height, callback) => {
  var sourceImage = new Image();

  sourceImage.onload = function() {
    // Create a canvas with the desired dimensions
    var canvas = document.createElement("canvas");
    canvas.width = width;
    canvas.height = height;

    // Scale and draw the source image to the canvas
    canvas.getContext("2d").drawImage(sourceImage, 0, 0, width, height);

    // Convert the canvas to a data URL in PNG format
    callback(canvas.toDataURL());
  }

  sourceImage.src = url;
}

cleanHTML = (input) => {

  if (!input) return "&nbsp;";

  var stringStripper = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g; 
  var output = input.replace(stringStripper, ' ');

  var commentStripper = new RegExp('<!--(.*?)-->','g');
  var output = output.replace(commentStripper, '');
  
  var tagStripper = new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi');  
  output = output.replace(tagStripper, '');

  var badTags = ['style', 'script','applet','embed','noframes','noscript'];
  for (var i=0; i< badTags.length; i++) {
    tagStripper = new RegExp('<'+badTags[i]+'.*?'+badTags[i]+'(.*?)>', 'gi');
    output = output.replace(tagStripper, '');
  }

  // 5. remove attributes ' style="..."'
  var badAttributes = ['start']; //'style', 
  for (var i=0; i< badAttributes.length; i++) {
    var attributeStripper = new RegExp(' ' + badAttributes[i] + '="(.*?)"','gi');
    output = output.replace(attributeStripper, '');
  }

  // 6. add centering to images
  //var imgStyle = '<img style="margin:auto;" ';
  //output = output.replace('<img ', imgStyle);

  return output;
}

pasteHtmlAtCaret = (html, selectPastedContent) => {
  var sel, range;
  if (window.getSelection) {
    sel = window.getSelection();
    if (sel.getRangeAt && sel.rangeCount) {
      range = sel.getRangeAt(0);
      range.deleteContents();

      // Range.createContextualFragment() 
      var el = document.createElement("div");
      el.innerHTML = html;
      var frag = document.createDocumentFragment(), node, lastNode;
      while ( (node = el.firstChild) ) {
          lastNode = frag.appendChild(node);
      }
      var firstNode = frag.firstChild;
      range.insertNode(frag);

      // Preserve the selection
      if (lastNode) {
          range = range.cloneRange();
          range.setStartAfter(lastNode);
          if (selectPastedContent) {
              range.setStartBefore(firstNode);
          } else {
              range.collapse(True);
          }
          sel.removeAllRanges();
          sel.addRange(range);
      }
    }
  } 
}

fast = (model, fields) => {
  if (!fields) return;

  var inputs = [];
  inputs.length = model.length;

  $.each(fields, (key, value) => {
    if ( model.indexOf(key) > -1 ) {

      field = {};  
      field.id = key;
      field.placeholder = key;
      field.value = value;

      field.type = typeof(value)=='string' ? 'text' : typeof(value);
      field.type = typeof(value)=='number' ? typeof(value) : field.type;
      field.type = typeof(value)=='boolean' ? typeof(value) : field.type;
      field.type = typeof(value.getMonth) === 'function' ? 'date' : field.type;
      field.type = (key.toLowerCase().includes("email")) ? 'email' : field.type ; 

      if (field.type == 'date'){
        field.text = moment(value, "YYYY-MM-DD").format("DD MMMM YYYY")
      }

      if (field.type == 'boolean'){
        if (value) {
          field.checked = 'checked'
        } else {
          field.checked = '';
        }
      }

      inputs[model.indexOf(key)] = field;
    }
  });
  return inputs;
}

systat = (variable, info, every) => {
  Meteor.call('shell', info, function(error,response) {
    if(error) {
      Session.set(variable, "Error:" + error.reason);
      return;
    }
    Session.set(variable, response);
  });

  if (every) { 
    clock = Meteor.setTimeout(function(){ systat(variable, info, every); }, every);
  }
}

dateDiff = (a, b) => {
    var d = Math.abs(a.getTime() - b.getTime()) / 1000;                        // delta
    var r = {};                                                                // result
    var s = {                                                                  // structure
        year: 31536000,
        month: 2592000,
        week: 604800, // uncomment row to ignore
        day: 86400,   // feel free to add your own row
        hour: 3600,
        minute: 60,
        second: 1
    };

    Object.keys(s).forEach(function(key){
        r[key] = Math.floor(d / s[key]);
        d -= r[key] * s[key];
    });
    
    return r;
};

updateQuestion = (field, event, instance) => {
  var field = event.target;
  var value = $(field).val();
  my = Meteor.user().profile;
  my.exams[Session.get("Project")].questions[Session.get("Item")][field.id] = value;
  Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
}

updateScore = () => {
  takeId = Session.get('id');
  testId = my.exams[Session.get('Project')]._id || 0;

  if (getSettings('multi')) {
    selected = Session.get('selected') || [];
    selected.forEach(function(item){
      record = {
          user: Meteor.userId()
        , test: testId
        , take: takeId
        , exam: Session.get('Project')
        , quiz: Session.get('Item')
        , pick: item
        , flag: Session.get('flagged') || False
        , time: Session.get('nao')
        , date: Session.get('time')
        , email: Meteor.user().profile.email
      }
      record.id = Results.insert(record);
      Meteor.call('updateScore', record);      
    })
  } else {
    record = {
        user: Meteor.userId()
      , test: testId
      , take: takeId
      , exam: Session.get('Project')
      , quiz: Session.get('Item')
      , pick: Session.get('selected')
      , flag: Session.get('flagged') || False
      , time: Session.get('nao')
      , date: Session.get('time')
      , email: Meteor.user().profile.email
    }
    record.id = Results.insert(record);
    Meteor.call('updateScore', record);
  }
}

stopWatch = () => {
  Meteor.clearTimeout(timer);  
  clearTimeout(timer);
}

nextQuestion = () => {

  stopWatch();
  
  if (Session.get('Page') != 'test') return;

  updateScore();

  // set next 
  var last = my.exams[Session.get('Project')].questions.length-1  
  item = (Session.get('Item')+1).clamp(0, last);
  Session.set('Item', item);

  Session.set('selected', undefined);
  Session.set('flagged', undefined);

  timeout = getSettings('timeout')*1000;
  if (last == item) {
    d = new Date(new Date().getTime() + timeout); 
    Session.set('nao', d);
    timer = Meteor.setTimeout(function() { updateScore(); Session.set('Page', 'done');}, timeout); 
  } else {
    if (timeout){
      d = new Date(new Date().getTime() + timeout); 
      Session.set('nao', d);
      timer = Meteor.setTimeout(function() { nextQuestion(); }, timeout);
    }
  }
  
}

clickNext = () => {
  var item = Session.get('Item');
  var last = my.exams[Session.get('Project')].questions.length-1
  if (last == item) {
    updateScore(); 
    stopWatch();      
    Session.set('Page', 'done');
  } else {
    nextQuestion();
  }        
}

getSettings = (setting) => {
  
  var x = my.exams[Session.get('Project')].settings || app.settings;
  var y = my.exams[Session.get('Project')].questions[Session.get('Item')].settings;
  var z = $.extend(True, {}, x, y);
  
  if (setting) {
    return z[setting];
  } else {
    return z;
  }
}

tick = (callback) => {
  TIMER = Session.get('time');
  if (TIMER > 0) {
    TIMER--;
    Session.set("time", TIMER);
    return TIMER;
  } else {
    return callback();
  }
};

saveDoc = (value) => {

  my = Meteor.user().profile;
  my.exams[Session.get("Project")].questions[Session.get("Item")].question = value;
  Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});      
}

getList = (list, kind) => {
  
  entries = Lists.find({ kind : kind }, { sort: { rank: -1} }).fetch();
  extra=entries.map((function(item) {return item.title;}));
  cookie = kind.toUpperCase() + 'S'
  Session.set(cookie, _.unique(list.concat(extra).sort()));
    
  return Session.get(cookie);
}

getTotal = (questions) => {
  var total = 0;
  questions.forEach(
    (question) => {
      question.answers.forEach(
      (answer) => { 
        total += parseInt(answer.points);
      });
    })
  return total;
}

///////////////////////////////////////////////////////////////////////////////
// APP DATA
Template.registerHelper('equals', 
  (a, b) => {
    return (a === b);
  }
);

Template.registerHelper('can', 
  () => {
    return Meteor.user() && Meteor.user().isAdmin;
  }
);

Template.registerHelper('read', 
  (data) => {
    if (data && data.length) {
      $.each(data, function (position, object) {
        object.curocc = position+1;
        data[position] = object;
      });
      return data;
    }
  }
);

Template.registerHelper('contains', 
  (list, item) => {
    return (list.indexOf(item) != -1);
  },
);

Template.registerHelper('Session', 
  (variable) => {
    return Session.get(variable);
  }
);

Template.registerHelper('initcaps', 
  (string) => {
    if (string) return string.charAt(0).toUpperCase() + string.substring(1).toLowerCase();
  }
);

Template.registerHelper('offset', 
  (n) => { 
    n = n + 1
    return n;
  }
);

Template.registerHelper('scrollbar', 
  (height, rows, page) => {
    if (rows > page){
      return `height:${height}vh;width:100%;overflow:hidden;overflow-y:scroll;`;
    }
  }
);

Template.registerHelper('cleanHTML', 
  (input) => {
    // 1. remove line breaks / Mso classes
    var stringStripper = /(\n|\r| class=(")?Mso[a-zA-Z]+(")?)/g; 
    var output = input.replace(stringStripper, ' ');

    // 2. strip Word generated HTML comments
    var commentStripper = new RegExp('<!--(.*?)-->','g');
    var output = output.replace(commentStripper, '');

    // 3. remove tags leave content if any
    var tagStripper = new RegExp('<(/)*(meta|link|span|\\?xml:|st1:|o:|font)(.*?)>','gi');
    output = output.replace(tagStripper, '');

    // 4. Remove everything in between and including tags '<style(.)style(.)>'
    var badTags = ['style', 'script','applet','embed','noframes','noscript'];
    for (var i=0; i< badTags.length; i++) {
      tagStripper = new RegExp('<'+badTags[i]+'.*?'+badTags[i]+'(.*?)>', 'gi');
      output = output.replace(tagStripper, '');
    }

    // 5. remove attributes ' style="..."'
    // var badAttributes = ['style', 'start'];
    // for (var i=0; i< badAttributes.length; i++) {
    //  var attributeStripper = new RegExp(' ' + badAttributes[i] + '="(.*?)"','gi');
    //  output = output.replace(attributeStripper, '');
    //}
    // 6. add centering to images
    //var imgStyle = '<img style="margin:auto;" ';
    //output = output.replace('<img ', imgStyle);
    
    return output;
  }

);

Template.registerHelper('Page',
  () => {
    page = Session.get("Page");
    if (Meteor.user() && page == 'login') {
      page = LANDING_PAGE
    }
    //location.pathname = '/';
    location.hash = '#' + page;
    return page;
  }
);

Template.registerHelper('avatar', 
  (email) => {
    if (email) {
      user = Meteor.users.findOne({ 'profile.email': email });
      if (user){
        return user.profile.image || 'http://lorempixel.com/175/175/';
      } else {
        return 'http://lorempixel.com/175/175/';
      }
    } else {
      return Meteor.user().profile.image || 'http://lorempixel.com/175/175/';
    }
  }
);

Template.registerHelper('fitText', 
  (string) => {
    if (string) {
      var size = 50 - ((string.length - 6)*6);
      var tag = '<span style="font-size:'+size.clamp(16, 64)+'px">'+string+'</span>'
      return tag;
    }
  }
);  

Template.registerHelper('score',
  () => {
    // map reduce sum aggregrates
    try {
      return Results.find({ take: Session.get('id') }).fetch().map(item => parseInt(item.points) || 0).reduce((a, b) => a + b);
    } catch (error) {
      //return sum(Results.find({ take: Session.get('id') }).fetch(), 'points');
    } 
  }
);  

// TODO should not relay points to the client just the total
Template.registerHelper('total',
  () => {
    var total = 0;
    if (my) {
      my.exams[Session.get('Project')].questions.forEach(
        (question) => {
          question.answers.forEach(
          (answer) => { 
            total += parseInt(answer.points);
          });
        })
      return total;
    }
  }
);

nao = () => {
  
  ts = Session.get("time") || new Date();                                                                                           // 459
  ft = Session.get('nao') || new Date();                                                                                            // 460
  r = dateDiff(ft, ts);
  if (ts < ft) {
    return (r.minute < 10 ? "0" : "") +  r.minute + ":" + (r.second < 10 ? "0" : "") + r.second;
  } else {
    return "Time up";
  }
  
}

Template.registerHelper('nao', nao);  

// GLOBAL DATA
project = () => {
  my = Meteor.user().profile;  
  return my.exams[Session.get('Project')];
};

questions = () => {
  my = Meteor.user().profile;  
  if (my.exams[Session.get('Project')]){
    return my.exams[Session.get('Project')].questions;
  };
};

team = () => {
  my = Meteor.user().profile;  
  return my.teams[Session.get('Team')];
};

Template.registerHelper('project', project);  
Template.registerHelper('questions', questions);  
Template.registerHelper('team', team);  

///////////////////////////////////////////////////////////////////////////////
// BODY EVENTS
Template.body.created = () => {
  ['Project','Item', 'Team', 'Name', 'showCode'].forEach((variable) => { Session.set(variable, 0); });
}

Template.body.rendered = () => { 

  $('#login-name-link').hide();    
  if (Meteor.user() == null) {
    Session.set("Page", "login");
    Accounts._loginButtonsSession.set('dropdownVisible', True);
  } else {
    Session.set("Page", "home");
    Accounts._loginButtonsSession.set('dropdownVisible', False);
  }  

}

///////////////////////////////////////////////////////////////////////////////
// MENU DATA
Template.menu.helpers({
  'options':() => {
    return ['team','exam','test','push','dash','help']; 
  }
});

// MENU EVENTS
Template.menu.events({
  'click #menu':(event, instance) => {
    event.preventDefault();
    var x = document.getElementById("menu");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
  },
  'click .link':(event, instance) => {
    event.preventDefault();
    var field = event.target;
    Session.set('Page', field.id); 
  },  
  'click .logout':(event, instance) => {
    event.preventDefault();
    Meteor.logout(function(error){
      if ( error ){
        alert('Error logging out: '+error); 
      } else {
        Object.keys(Session.keys).forEach(function(key){
          Session.set(key, undefined);
        });
        Session.keys = {}; // remove session keys
      }
      Accounts._loginButtonsSession.set('dropdownVisible', True);
      Session.set('Page', 'login');
    });
  },
  'click .login':(event, instance) => {
    event.preventDefault();
    Accounts._loginButtonsSession.set('dropdownVisible', True);
    Accounts._loginButtonsSession.set('inForgotPasswordFlow', False);
    Session.set('Page', 'login');
  },
  'click .changePassword':(event, instance) => {
    event.preventDefault();
    Accounts._loginButtonsSession.set('dropdownVisible', True);
    Accounts._loginButtonsSession.set('inChangePasswordFlow', True);
    Session.set('Page', 'login');
  },
  'click .forgotPassword':(event, instance) => {
    event.preventDefault();
    Session.set('Page', 'login');
    Accounts._loginButtonsSession.set('dropdownVisible', True);
    Accounts._loginButtonsSession.set('inForgotPasswordFlow', True);
  },
})

///////////////////////////////////////////////////////////////////////////////
// PROFILE DATA
Template.home.helpers({
  'user':() => {
    return Meteor.user().profile;
  },
  'form':() => {    
    var model = ['name','email','joined'];
    var fields = Meteor.user().profile;
    return fast(model, fields);
  },
  'exams':() => {
    my = Meteor.user().profile;
    return my.exams;
  },
  'teams':() => {
    my = Meteor.user().profile;
    return my.teams;
  }
});

// HOME EVENTS
Template.home.events({
  'change .field':(event, instance) => {
    var field = event.target;
    var value = $(field).val();

    if (field.type == 'number' ) {
      value = parseInt(value);
    }

    if (field.type == 'date' ) {
      $(field).attr("data-date", moment(value, "YYYY-MM-DD").format($(field).attr("data-date-format")))
      value = moment(value, "YYYY-MM-DD" ).toDate();
    }

    if (field.type == 'checkbox') {
      value = field.checked || False;
    }
        
    if (field.id == 'email' ) {
      Meteor.call("changeEmail", value, function (error, result) {
        if (result){
          my[field.id] = value;
          //Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});      
        }
      });
    } else {
      my[field.id] = value;
      Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});      
    }
  },
  "change input[type='file']":(event,template) => {
    var files=event.target.files;
    if(files.length===0){
      return;
    }
    var file=files[0];
    var fileReader=new FileReader();
    fileReader.onload=function(event){
      var dataUri=event.target.result;
      resizeImage(dataUri, 200, 200, function(png){ 
        Meteor.users.update({_id: Meteor.userId()}, {$set: { "profile.image": png}});
      }); 
    };
    fileReader.readAsDataURL(file);
  },
  'click #project':(event, instance) => {
    my = Meteor.user().profile;
    my.exams.push(app.exam);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Project', my.exams.length-1);
    Session.set('Item', 0);
    Session.set("Page", 'exam');
  },  
  'click .exam':(event, instance) => {
    Session.set("Project", parseInt(event.target.id));
    Session.set("Item", 0);
    Session.set("Page", 'exam');
  }, 
  'click #team':(event, instance) => {
    my = Meteor.user().profile;
    index = my.teams.push({ 'name':'New Team', 'email': 'group@oznz.me', 'members': [{'name':'New name','email':'changeme@oznz.me'}]});
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Team', index-1);
    Session.set('Name', 0);
    Session.set("Page", 'team');
  },  
  'click .team':(event, instance) => {
    Session.set("Team", parseInt(event.target.id));
    Session.set("Name", 0);
    Session.set("Page", 'team');
  }, 
});

///////////////////////////////////////////////////////////////////////////////
// TEAM DATA
Template.team.helpers({
  'form':() => {
    var model = ['name','email'];
    var fields = my.teams[Session.get('Team')];
    return fast(model, fields);
  },
  'members':() => {    
    my = Meteor.user().profile;
    return my.teams[Session.get('Team')].members;
  },
  'member':() => {
    var model = ['name','email'];
    var fields = my.teams[Session.get('Team')].members[Session.get('Name')];
    return fast(model, fields);
  },
});

// TEAM EVENTS
Template.team.events({
  'change .field': (event, instance) => {
    var field = event.target;
    var value = $(field).val();

    if (field.type == 'number' ) {
      value = parseInt(value);
    }
    
    if (field.type == 'date' ) {
      $(field).attr("data-date", moment(value, "YYYY-MM-DD").format($(field).attr("data-date-format")))
      value = moment(value, "YYYY-MM-DD" ).toDate();
    }
    
    my.teams[Session.get('Team')][field.id] = value;
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
  },
  'change .member': (event, instance) => {
    var field = event.target;
    var value = $(field).val();

    if (field.type == 'number' ) {
      value = parseInt(value);
    }
    if (field.type == 'date' ) {
      $(field).attr("data-date", moment(value, "YYYY-MM-DD").format($(field).attr("data-date-format")))
      value = moment(value, "YYYY-MM-DD" ).toDate();
    }
    
    my.teams[Session.get("Team")].members[Session.get("Name")][field.id] = value;
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
  },
  'click #member':(event, instance) =>{
    my = Meteor.user().profile;
    index = my.teams[Session.get('Team')].members.push({'name':'name', 'email': 'change@oznz.me'});
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Name', index-1);
    $("input#name.member").focus();
  },  
  'click .team':(event, instance) =>{
    Session.set("Name", parseInt(event.target.id));
    $("input#name.member").focus();
  },
  'click #delgrp':(event, instance) => {
    my = Meteor.user().profile;
    my.teams.splice(Session.get("Team"),1);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Team', 0);
  },     
  'click #delusr':(event, instance) => {
    my = Meteor.user().profile;
    my.teams[Session.get("Team")].members.splice(Session.get("Name"),1);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Name', 0);
  },     
});

///////////////////////////////////////////////////////////////////////////////
// EXAM DATA
Template.exam.helpers({
  'form':() =>  {
    var model = ['name','type','level','field','course','module','award'];
    var fields = my.exams[Session.get('Project')];
    return fast(model, fields);
  },
});

// EXAM EVENTS
Template.exam.events({
  'change .field':(event, instance) => {
    var field = event.target;
    var value = $(field).val();

    number = parseFloat(value);
    if (number){
      field.type = 'number';
      value = number;
    } 

    if (field.type == 'date') {
      $(field).attr("data-date", moment(value, "YYYY-MM-DD").format($(field).attr("data-date-format")))
      value = moment(value, "YYYY-MM-DD" ).toDate();
    }

    if (field.type == 'checkbox') {
      value = field.checked || False;
    }
    
    my = Meteor.user().profile;
    my.exams[Session.get('Project')][field.id] = value;
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
  },  
  'click #delete':(event, instance) => {
    my = Meteor.user().profile;
    my.exams.splice(Session.get("Project"),1);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Project', 0);
    Session.set('Page', 'home');
  },  
  'click #new':(event, instance) => {
    my = Meteor.user().profile;
    my.exams[Session.get("Project")].questions.push(app.question);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Item', my.exams[Session.get("Project")].questions.length-1);
    Session.set('Page', 'edit');
  },  
  'click .item':(event, instance) => {
    Session.set("Item", parseInt(event.target.id));
    Session.set("Page", 'edit');
  },   
});


///////////////////////////////////////////////////////////////////////////////
// EDIT DATA
Template.edit.helpers({
  'questions':() =>  {
    my = Meteor.user().profile;  
    if (my.exams[Session.get('Project')]){
      return my.exams[Session.get('Project')].questions;
    };
  },  
  'slide':() =>  {
    my = Meteor.user().profile;  
    if (my.exams[Session.get('Project')]){
      return my.exams[Session.get("Project")].questions[Session.get("Item")].question;
    };
  },  
});

///////////////////////////////////////////////////////////////////////////////
// EDIT EVENTS
Template.edit.events({
  'click .exam':(event, instance) => {
    Session.set("Project", parseInt(event.target.id));
    //Session.set("Item", 0); this is better
    Session.set("Page", 'exam');
  }, 
  'click #back':(event, instance) => {
    var x = Session.get('Item')-1;
    Session.set('Item', x.clamp(0, my.exams[Session.get('Project')].questions.length));
  },  
  'click #trash':(event, instance) => {
    index = Session.get("Item");
    my.exams[Session.get("Project")].questions.splice(index, 1);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    last = my.exams[Session.get("Project")].questions.length-1;
    Session.set('Item', index.clamp(0, last));
  },  
  'click #swap':(event, instance) => {
    var quiz = questions();
    last = quiz.length-1;
    a = Session.get("Item");
    if (a == 0) { 
      b = last
    } else {
      b = (a-1).clamp(0, last);  
    };
    quiz[a] = quiz.splice(b, 1, quiz[a])[0];  
    my.exams[Session.get("Project")].questions = quiz;
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Item', b);
  },  
  'click #html':(event, instance) => {
    Session.set('showCode', 1 - Session.get('showCode'));
  },  
  'click #cogs':(event, instance) => {
    Session.set("Page", 'cogs');
  }, 
  'click #new':(event, instance) => {
    my = Meteor.user().profile;
    my.exams[Session.get("Project")].questions.push(app.question);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Item', my.exams[Session.get("Project")].questions.length-1);
  },
  'click #next':(event, instance) => {
    var x = Session.get('Item')+1;
    var last = my.exams[Session.get('Project')].questions.length-1
    Session.set('Item', x.clamp(0, last));
  },  
  'change #topic':(event, instance) => {
    updateQuestion('topic', event, instance);
  },
  'blur #question':(event, instance) => {
    console.debug('%s edit blur #question ', event.target.id);
    if (Session.get('showCode')) {
      var value = $("#question").text();
    } else {
      var value = $("#question").html();
    }
    saveDoc(value);
  },
  'paste #question':(event, instance) => {
    event.preventDefault();
    var newData = cleanHTML(event.originalEvent.clipboardData.getData("text/html"))
    pasteHtmlAtCaret(newData, True);

    value = $('#question').html();
    my = Meteor.user().profile;
    my.exams[Session.get("Project")].questions[Session.get("Item")].question = value;
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    $('#question').html('');    
  },
  'change #title':(event, instance) => {
    updateQuestion('title', event, instance);
  },
  'change .answer':(event, instance) => {
    var field = event.target;
    var value = $(field).val();
    if (field.id[4] != '_'){
      my = Meteor.user().profile;
      my.exams[Session.get("Project")].questions[Session.get("Item")].answers[field.id][field.name] = value;
      Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    } else {
      if (field.id == 'new__text'){
        var points = parseInt($('#new__pts').val());
        my.exams[Session.get("Project")].questions[Session.get("Item")].answers.push({'option': value, 'points': points})
        $('#new__text').val("");
        $('#new__pts').val("");
        Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
        $('#new__text').focus();
      }
    }
  },
  'click .delete':(event, instance) => {
    var occ = event.target.id;
    my.exams[Session.get("Project")].questions[Session.get("Item")].answers.splice(occ, 1);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});    
  },
});

///////////////////////////////////////////////////////////////////////////////
// COGS DATA
Template.cogs.helpers({
  'form':() =>  {
    var model = ['back','next','topic','auto','timeout'];
    var fields = getSettings(); 
    return fast(model, fields);
  },  
  'multi':() =>  {
    var model = ['multi','min','max','rank'];
    var fields = getSettings(); 
    return fast(model, fields);
  },  
});

// COGS EVENTS
Template.cogs.events({
  'change .field': (event, instance) => {
    var field = event.target;
    var value = $(field).val();

    if (field.type == 'number' ) {
      value = parseInt(value);
    }
    if (field.type == 'date' ) {
      $(field).attr("data-date", moment(value, "YYYY-MM-DD").format($(field).attr("data-date-format")))
      value = moment(value, "YYYY-MM-DD" ).toDate();
    }
    if (field.type == 'checkbox') {
      value = field.checked || False;
    }
    // nesting yuk
    my.exams[Session.get('Project')].questions[Session.get('Item')].settings[field.id] = value;
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
  },
  'click .exam':(event, instance) => {
    Session.set("Project", parseInt(event.target.id));
    Session.set("Page", 'edit');
  }, 
  'click #edit':(event, instance) => {
    Session.set("Page", 'edit');
  }, 
  'click #delset':(event, instance) => {
    my = Meteor.user().profile;
    my.exams[Session.get('Project')].questions[Session.get('Item')].settings = {};
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
  },     
  'click #_back':(event, instance) => {
    var x = Session.get('Item')-1;
    Session.set('Item', x.clamp(0, my.exams[Session.get('Project')].questions.length));
  },  
  'click #trash':(event, instance) => {
    index = Session.get("Item");
    my.exams[Session.get("Project")].questions.splice(index, 1);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    last = my.exams[Session.get("Project")].questions.length-1;
    Session.set('Item', index.clamp(0, last));
  },  
  'click #swap':(event, instance) => {
    var quiz = questions();
    last = quiz.length-1;
    a = Session.get("Item");
    b = (a-1).clamp(0, last);
    quiz[a] = quiz.splice(b, 1, quiz[a])[0];  
    my.exams[Session.get("Project")].questions = quiz;
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Item', b);
  },  
  'click #new':(event, instance) => {
    my = Meteor.user().profile;
    my.exams[Session.get("Project")].questions.push(app.question);
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Item', my.exams[Session.get("Project")].questions.length-1);
  },
  'click #_next':(event, instance) => {
    var x = Session.get('Item')+1;
    var last = my.exams[Session.get('Project')].questions.length-1
    Session.set('Item', x.clamp(0, last));
  },  
});

///////////////////////////////////////////////////////////////////////////////
// TEST DATA
Template.test.helpers({
  'settings':() => {
    return getSettings();    
  },
  'selected':(occ) => {
    var selected = Session.get('selected') || [];
    if (getSettings('multi')) {
      if (selected.indexOf(String(occ)) == -1) { // Stringify occ here for '@index'
        return 'select'
      } else {
        return 'selected'        
      }
    } else {
      if (Session.get('selected') == occ) {
        return 'selected'
      } else {
        return 'select';
      }
    }
  }
});

// TEST EVENTS
Template.test.created = () => { 
  console.debug('%s test created', step++);

  Session.set('selected', undefined);
  Session.set('flagged', undefined);
  Session.set('Item', 0);

  timeout = getSettings('timeout')*1000;
  d = new Date(new Date().getTime() + timeout); 
  Session.set('nao', d);

  if (my.exams[Session.get('Project')].questions.length > 1) {
    timer = Meteor.setTimeout(function() { nextQuestion(); }, timeout);
  } else {
    timer = Meteor.setTimeout(function() { updateScore(); Session.set('Page', 'done');}, timeout);
  }

}

Template.test.rendered = () => { 
  var id = Random.id();
  Session.set('id', id);
}

Template.test.destroyed = () => { 
  stopWatch();
}

Template.test.events({
  'click #back':(event, instance) => {
    var x = Session.get('Item')-1;
    Session.set('Item', x.clamp(0, my.exams[Session.get('Project')].questions.length));
  },  
  'click #next':(event, instance) => {
    var multi = getSettings('multi');
    if (multi) {
      if (getSettings('min')-1 < $('.selected').length && $('.selected').length < getSettings('max')+1) {
        nextQuestion();          
      } else {
        $('.answer').fadeTo(100, 0.3, function() { $(this).fadeTo(500, 1.0); }); 
      }
    } else {
      if (Session.get('selected')){
        nextQuestion();
      } else {
        $('.answer').fadeTo(100, 0.3, function() { $(this).fadeTo(500, 1.0); }); 
      }
    }
  },  
  'click #done':(event, instance) => {
    if (Session.get('selected')){
      updateScore();
      stopWatch();      
      Session.set('Page', 'done');
    } else {
      $('.answer').fadeTo(100, 0.3, function() { $(this).fadeTo(500, 1.0); });       
    }
  },  
  'click #clock':(event, instance) => {
    Session.set('flagged', True);
    clickNext();
  },  
  'click .option':(event, instance) => {
    if (nao != 'Time up') {
      var occ = event.target.id;

      var multi = getSettings('multi');
      if (multi) {
        selected = Session.get('selected') || [];
        if (selected.indexOf(occ) == -1) {
          selected.push(occ);
        } else {
          selected.splice(selected.indexOf(occ),1);
        } 
        Session.set('selected', selected);
      } else {
        Session.set('selected', occ);
      }

      var auto = getSettings('auto');
      if (auto) {
        if (getSettings('min')-1 < selected.length && selected.length < getSettings('max')+1) {
          clickNext();
        }                
      }
    }    
  },
});

///////////////////////////////////////////////////////////////////////////////
// PUSH DATA
Template.push.helpers({
  'form':() =>  {
    if (Session.get('Test')) {
      test = Tests.findOne(Session.get('Test'));
    } else {
      test = Tests.findOne({});
    }
    if (test) {
      Session.set('Test', test._id);
    }

    var model = ['template','tries','expires','public', 'random', 'count'];
    return fast(model, test);
  },
  'projects':() => {
    var projectList = my.exams.map((function(item) {return item.name;}));
    return projectList;
  },
  'teams':() => {
    teamList = my.teams.map((function(item) {return item.name;}));
    return teamList;
  },
  'tests':() => {
    return Tests.find({}).fetch();
  },
  'test':() => {
    testId = Session.get('Test');    
    return Tests.find({ _id: testId}).fetch()[0];
  },
});

///////////////////////////////////////////////////////////////////////////////
// PUSH EVENTS
Template.push.events({
  'change .field':(event, instance) => {
    console.debug('%s change .field', event.target.id);

    var field = event.target;
    var value = $(field).val();

    if (field.type == 'number' ) {
      value = parseInt(value);
    }
    if (field.type == 'checkbox') {
      value = field.checked;
    }
    if (field.type == 'date' ) {
      $(field).attr("data-date", moment(value, "YYYY-MM-DD").format($(field).attr("data-date-format")))
      value = moment(value, "YYYY-MM-DD" ).toDate();
    }

    if (field.id == 'exam'){
      var projectList = my.exams.map((function(item) {return item.name;}));
      exam = my.exams[projectList.indexOf(event.target.value)];
      if (exam){
        test = Tests.findOne(testId);
        delete exam.name;
        $.extend(test, exam);
        delete test._id;
        Tests.update({'_id':testId}, { $set: test });
      }
    }
    if (field.id == 'team'){
      var teamList = my.teams.map((function(item) {return item.name;}));
      team = my.teams[teamList.indexOf(event.target.value)];
      if (team){
        test = Tests.findOne(testId);
        delete team.name;
        $.extend(test, team);
        delete test._id;
        Tests.update({'_id':testId}, { $set: test });
      }
    }

    Meteor.call('saveTest', Session.get('Test'), field.id, value);
  },
  'click #new':(event, instance) =>{
    console.debug('%s click #new', event.target.id);

    my = Meteor.user().profile;
    test = { 'name': project().name
           , 'user': Meteor.userId()
           , 'expires': new Date()
           , 'template': 'invite'
           , 'public': True
           , 'tries': 1
           , 'count': 10
           , 'random': False };

    exam = my.exams[Session.get("Project")];
    test = $.extend(True, {}, exam, test);
    team = my.teams[Session.get("Team")];
    test = $.extend(True, {}, team, test);
    
    testId = Tests.insert(test);    
    if (my.tests){
      my.tests.push(testId);
    } else {
      my.tests = [testId];
    }
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
    Session.set('Test', testId);
    $("input#name.test").focus();

  },  
  'click .test':(event, instance) =>{
    console.debug('%s click .test', event.target.id);

    Session.set("Test", event.target.id);
    $("input#name.test").focus();
  },
  'click #deltst':(event, instance) => {
    console.debug('%s click #deltst', event.target.id);

    testId = Session.get('Test');
    if (testId) {
      Tests.remove(testId);
    }
  },
  'click #send':(event, instance) =>{
    console.debug('%s click #send', event.target.id);
    event.preventDefault();

    //WOW THIS SUX fix this bullshit!!!
    if (Session.get('Test')){
      var name = Tests.find({ _id: Session.get('Test')}).fetch()[0].team;
      if (name) {
        my = Meteor.user().profile
        my.teams.forEach((team) => { 
          if (team.name == name) {
            team.members.forEach((user) => {
              Meteor.call('getLogin', user.email, {returnStubValue: True});
              console.log('%s sending test to ', user.email)
            });
          }
        });
      }
    }
  },
});

getResults = () => {

  Marks.remove({});
  userId = Meteor.userId();
  Meteor.call("showResults", userId, function (error, results) {
    console.debug('dash created showResults');

    results.forEach((result) => {
      var test = Tests.findOne(result._id.test);
      if (test) {
        // TODO process Results
        result.score = Results.find({ take: result._id.take }).fetch().map(item => parseInt(item.points) || 0).reduce((a, b) => a + b);
        result.total = getTotal(test.questions);
        result.grade = Math.round((result.score/result.total)*100);
        mark = Marks.insert({
            'name': test.name
          , 'test': result._id.test
          , 'take': result._id.take
          , 'email': result._id.email
          , 'score': result.score
          , 'total': result.total
          , 'grade': result.grade
        });
      }
    });
    Session.set('Mark', mark._id);
    Session.set('results', results);
  });

}
///////////////////////////////////////////////////////////////////////////////
// DASH DATA
Template.dash.created = () => { 
  console.debug('%s dash created', step++);  
  getResults();  
}

Template.dash.helpers({
  'enough':(points) => {
    console.debug('%s dash helper correct', step++);  
    if (parseInt(points) > 0){
      return True;
    } else {
      return False;
    }
  },
  'marks':() => {
    console.debug('%s dash helper marks', step++);  
    results = Session.get('results');
    return Marks.find().fetch();
  },
  'tests':() => {
    console.debug('%s dash helper tests', step++);  

    if (Session.get('Mark')) {
      mark = Marks.findOne(Session.get('Mark'));
    } else {
      mark = Marks.findOne({});
    }
    if (mark) {
      Session.set('Mark', mark._id);
      return Results.find({take: mark.take },{sort: { quiz: 1}}).fetch();
    }
  },
});

///////////////////////////////////////////////////////////////////////////////
// DASH EVENTS
Template.dash.events({
  'click .test':(event, instance) =>{
    console.debug('%s dash click .test', step++);  
    Session.set("Mark", event.target.id);
  },
  'click .answer':(event, instance) =>{
    console.debug('%s dash click .answer', step++);
    mark = Marks.findOne(Session.get("Mark"));
    take = Results.findOne({take: mark.take });
    Session.set('Project', take.exam);
    Session.set("Item", parseInt(event.target.id));
    Session.set("Page", 'edit');
  },
  'click #archive':(event, instance) =>{
    console.debug('%s dash click #archive', step++);      
    mark = Marks.findOne(Session.get("Mark"));
    var FileSaver = require('file-saver');
    var blob = new Blob([JSON.stringify(mark)], {type: "text/plain;charset=utf-8"});
    FileSaver.saveAs(blob, mark.name);
  },
  'click #delete':(event, instance) =>{
    console.debug('%s dash click #delete', step++);  
    mark = Marks.findOne(Session.get("Mark"));
    if (mark) {
      takeId = mark.take;
      if (takeId){
        Meteor.call('removeResults', takeId);
      }
      getResults();
    }
    Session.set('results', null);
    Session.set('Marks', null);
  },
});

// TRACKER 
Tracker.autorun(function(){
  if (Meteor.user()) {
    my = Meteor.user().profile;
  }
});

Meteor.startup(() => {
 
  Meteor.setInterval(function () {
    Session.set("time", new Date); // use local time, TODO: fix 
  }, 1000);

});

