import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { Random } from 'meteor/random';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';

import sys from 'sys'
import { exec } from 'child_process';

////////////////////////////////////////////////////////////////////////////////////////////////
shell = (command) => {

  var Future = Npm.require( 'fibers/future' ); 
  
  // Create our future instance.
  var future = new Future();

  // executes ``
  exec(command, function (error, stdout, stderr) {
    //console.log(stdout);
    if ( error ) {
      console.log('stderr:%s exec error:%s',stderr, error);
      var lines = stderr.toString().split('\n');
    } else {
      var lines = stdout.toString().split('\n');
    }
    future.return( lines );
  });

  return future.wait();  
}

// Pass in the objects to merge as arguments.
// For a deep extend, set the first argument to `true`.
extend = function () {

    // Variables
    var extended = {};
    var deep = false;
    var i = 0;
    var length = arguments.length;

    // Check if a deep merge
    if ( Object.prototype.toString.call( arguments[0] ) === '[object Boolean]' ) {
        deep = arguments[0];
        i++;
    }

    // Merge the object into the extended object
    var merge = function (obj) {
        for ( var prop in obj ) {
            if ( Object.prototype.hasOwnProperty.call( obj, prop ) ) {
                // If deep merge and property is an object, merge properties
                if ( deep && Object.prototype.toString.call(obj[prop]) === '[object Object]' ) {
                    extended[prop] = extend( true, extended[prop], obj[prop] );
                } else {
                    extended[prop] = obj[prop];
                }
            }
        }
    };

    // Loop through each object and conduct a merge
    for ( ; i < length; i++ ) {
        var obj = arguments[i];
        merge(obj);
    }

    return extended;

};

withTypes = (key, value) => {
  if (typeof value === 'string') {
    if (moment(value).isValid()) {
      return new Date(value);
    } else {
      number = parseFloat(value);
      if (number) {
        return number;
      }
    }
  }
  return value;
}

////////////////////////////////////////////////////////////////////////////////////////////////
Exams = new Mongo.Collection("exams");
Meteor.publish('exams', function () {
  return Exams.find({ 'user': this.userId });
});

Tests = new Mongo.Collection("tests");
Meteor.publish('tests', function () {
  if (this.userId) {
    return Tests.find({'user': this.userId });
  }
});

Results = new Mongo.Collection("results");
Meteor.publish('results', function () {
  return Results.find();
});

// lookups selects
Lists = new Mongo.Collection('lists');
Meteor.publish('lists', function () {
  return Lists.find();
});

// user data
Meteor.publish('users', function () {
  return Meteor.users.find();
});

Meteor.publish(null, function() {
  return Meteor.users.find({}, {fields: {username: 1, name: 1, profile: 1}});
});  

// Permissions
Meteor.users.allow({
  insert: function (userId, doc) {
    // only admin can insert 
    var u = Meteor.users.findOne({_id:userId});
    return (u && u.isAdmin);
  },
  update: function (userId, doc, fields, modifier) {
    if (userId && doc._id === userId) {
      console.log("user allowed to modify own account!");
      // user can modify own 
      return true;
    }
    // admin can modify any
    var u = Meteor.users.findOne({_id:userId});
    return (u && u.isAdmin);
  },
  remove: function (userId, doc) {
    // only admin can remove
    var u = Meteor.users.findOne({_id:userId});
    return (u && u.isAdmin);
  }
});

Exams.allow({
  insert: function (userId, doc) {
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    return true;
  },
  remove: function (userId, doc) {
    return true;
  }
});

Tests.allow({
  insert: function (userId, doc) {
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    return true;
  },
  remove: function (userId, doc) {
    return true;
  }
});

Results.allow({
  insert: function (userId, doc) {
    return true;
  },
  update: function (userId, doc, fields, modifier) {
    return true;
  },
  remove: function (userId, doc) {
    return true;
  }
});

// Permissions


Accounts.config({
  sendVerificationEmail: true,
  forbidClientAccountCreation: false
});

///  T R I G G E R S  ==================================================
Accounts.onCreateUser(function(options, user) {
    
    if (user.username == 0) {
      user.username = user.emails[0].address.split('@')[0].replace('.','');
    }
    
    var userProperties = {
      profile: options.profile || {},
      isAdmin: user.isAdmin
    };
    
    user = _.extend(user, userProperties);

    if (options.email) {
      user.profile.email = options.email;
    }
    
    if (!user.profile.name) {
      user.profile.name = user.username;
    }
        
    if (!Meteor.users.find().count() ) {
      user.isAdmin = true;
    }
  
    if (options.profile) {
      user.profile = options.profile;
    }
    
    user.profile.exams = [];
    user.profile.teams = [];
    user.profile.tests = [];
    
    user.profile.joined = new Date();

    return user;
    
});

///  S E R V I C E S   =================================================
Meteor.methods({
  'version':() => {
    console.log("showbob 1.10");
  },
  'changeEmail':(newEmail) => {
    var error=[];
    if (Meteor.users.findOne({'emails.address': newEmail, id: {$not: Meteor.userId}})) {
      error.push({name: "email", type: "exist"});
      console.log('Error duplicate mail not allowed');
      throw new Meteor.Error(422, error);
    }
    
    us = Meteor.users.find({ _id: Meteor.userId() }).fetch()[0];
    email = us.emails.pop(); 
    us.emails.push({ 'address': newEmail, 'verified':false });
    status = Meteor.users.update({ '_id': Meteor.userId() }, { $set: { 'emails': us.emails, 'profile.email': newEmail }});    
    if (status) {
      console.log('Email set to %s.', newEmail);
      status = Accounts.sendVerificationEmail(Meteor.userId());
    }
    return status; 
  },
  'changeAvatar':(userId, imageURL) => {
    return Meteor.users.update({_id: Meteor.userId()}, {$set: { "profile.image": imageURL}});
  },
  'setTest':(testId, newValue) => {
    return Tests.update({_id: testId}, {$set: newValue });
  },
  'removeTests':(testId) => {
    console.log("Parting is such sweet sorrow");
    currentUser = Meteor.users.findOne({_id : Meteor.userId()});
    if (currentUser.isAdmin) {
      if (testId) {
        return Tests.remove({"_id": testId});
      } else {
        return Tests.remove({});      
      }
    } else {
      console.log("Closhe but no shigar");
    }
  },
  'saveDoc':(Project, Item, HTML) => {
    my = Meteor.user().profile;
    my.exams[Project].questions[Item].question = HTML;
    Meteor.users.update(Meteor.userId(), {$set: {"profile": my }});
  },
  'saveTest':(testId, field, value) => {
    var result = {}; result[field] = value;
    Tests.update({_id :testId}, {$set:  result }, function(error, affectedDocs) {
      if (error) {
        throw new Meteor.Error(500, error.message);
      } else {
        console.log("Update Successful");
      }
    });
  },
  'shell':(command) => {
    return shell(command);
  },
  'getLogin': (email) => {
    console.log('showbob server, getLogin');
    var user = Meteor.users.find({'profile.email': email}).fetch()[0];
    if (user) {
      userId = user._id;
      console.log('Getting login for user %s', userId);
      Accounts.sendLoginEmail(userId, email);
    } else {
      var userId = Accounts.createUser({username: email.split('@')[0], email: email, password: Random.id()});
      Accounts.sendEnrollmentEmail(userId);
      console.log('New user %s', userId);
    }
  },
  'getCurrentTime':() => {
    //console.log('on server, getCurrentTime called');
    return new Date();
  },  
  'setAdmin':(user, isAdmin) => {
    Meteor.users.update({_id: user._id}, {$set: { 'isAdmin': isAdmin } });        
    console.log(isAdmin);
  },
  'updateScore':(record) => {
    try {
      // The Record 
      x = Results.find({ _id: record.id }).fetch()[0];      
      console.log('x::'+JSON.stringify(x));
      if (record.user == Meteor.userId()) {
        exam = Meteor.user().profile.exams[record.exam];
        console.log('exam::'+JSON.stringify(record.exam));
      } else {
        try {
          exam = Tests.find(record.test).fetch()[0];
        } catch (error) {
          console.error(error.message);
        }
      }
      y = exam.questions[record.quiz].answers[record.pick];

      result = extend(true, {}, x, y);
      console.log(JSON.stringify(result));

      delete result._id; //to avoid update _id minimongo
      Results.update({ _id: record.id }, {$set: result });

      // sum aggregrates
      alles = Results.find({ take: record.take }).fetch().map(item => parseInt(item.points) || 0).reduce((a, b) => a + b);
      console.log(JSON.stringify(alles, null, 2));
    }
    catch(error) {
      console.error(error.message);
    }
  }, // is this still a thing? Argh, yes, yes it is.
  'setList': function (kind) {

      if (kind == null) {
        Lists.remove({});
        return console.log('All lists removed');
      }

      Lists.remove({ 'kind': kind });

      var pipeline = [{
          $group: {
              _id: "$"+kind,
              childId: { $first: "$_id" }
          }
      }]

      childIds = Results.aggregate(pipeline).map(function(child) { return child.childId });

      items = Results.find({_id: {$in: childIds}}).fetch();
      items = _.unique(items);
      _(items).each(function(item) {
        Lists.insert({'kind': kind, 'title': item[kind]});
      });

  },
  'removeResults':(takeId) => {
    console.log("Nothing to see here");
    currentUser = Meteor.users.findOne({_id : Meteor.userId()});
    if (currentUser.isAdmin) {
      if (takeId) {
        return Results.remove({"take": takeId});
      } else {
        return Results.remove({}); 
      }
    } else {
      console.log("Closhe but no shigar");
    }
  },
  'showResults':(userId) => {
    console.log('...have seen things');
    pipeline = [ { $match : {"user": userId} }, { $group : { _id : {email:'$email', test: '$test', take:'$take'}, 'score': { $sum : "$points"}}} ];
    summary = Results.aggregate(pipeline);
    console.log(summary);
    return summary;
  }  
});
