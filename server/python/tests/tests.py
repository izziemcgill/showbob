import bleach
import docx2txt
#import pdf2text

from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize

from PIL import Image
from geotext import GeoText

import os, re

def listfiles(directory, pattern):
    return [os.path.join(path, file)
            for (path, dirs, files) in os.walk(directory)
            for file in files
            if re.match(pattern, file)]

pattern = '.*\.docx$'
files = listfiles('/home/dad/Documents', pattern)[-10:]

from pprint import pprint
pprint(files)

for file in files:  
  text = docx2txt.process(file)
  sentences = sent_tokenize(text)
  words = word_tokenize(text)
  for sentence in sentences: places = GeoText(sentence).cities

  print(text)
  print(sentences)
  print(words)
  print(places)

