from lxml import html
import requests

## Get ALL authors

alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWZ'
for letter in alphabet:
  page = requests.get("https://en.wikipedia.org/wiki/List_of_authors_by_name:_%s" % letter)
  tree = html.fromstring(page.content)
  links = tree.xpath('//*[@id="mw-content-text"]/ul[*]/li[*]/a[1]')
  authors += [ author.get('title') for author in links ]
  
for author in authors: print(author)

## Get kids authors

page = requests.get("https://en.wikipedia.org/wiki/List_of_children%27s_literature_writers")
tree = html.fromstring(page.content)
links = tree.xpath('//*[@id="mw-content-text"]/ul[*]/li[*]/a[1]')
authors = [ author.get('title') for author in links ]
for author in authors: print(author)

## Get women authors

page = requests.get("https://en.wikipedia.org/wiki/List_of_women_writers")
tree = html.fromstring(page.content)
links = tree.xpath('//*[@id="mw-content-text"]/div[*]/ul[*]/li[*]/a[1]')
authors = [ author.get('title') for author in links ]
for author in authors: print(author)

