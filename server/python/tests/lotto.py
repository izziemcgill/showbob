## scrape lottery site for all numbers evar
import os, requests
from lxml import html

file = open('/home/dad/Desktop/lotto.csv', 'a')

for year in range(1994, 2018):
  # e.g. https://www.lottery.co.uk/lotto/results/archive-1995
  page = requests.get("https://www.lottery.co.uk/lotto/results/archive-%s" % year)
  tree = html.fromstring(page.content)
  cols = tree.xpath('//*[@id="siteContainer"]/div[2]/table/tr[*]')
  sel = cols[0]
  
  # columns
  dd = sel.xpath('//*/td[1]/a/text()')
  n1 = sel.xpath('//*/td[2]/div[1]/text()')
  n2 = sel.xpath('//*/td[2]/div[2]/text()')
  n3 = sel.xpath('//*/td[2]/div[3]/text()')
  n4 = sel.xpath('//*/td[2]/div[4]/text()')
  n5 = sel.xpath('//*/td[2]/div[5]/text()')
  n6 = sel.xpath('//*/td[2]/div[6]/text()')
  bn = sel.xpath('//*/td[2]/div[7]/text()')
  jp = sel.xpath('//*/td[3]/strong/text()')

  # read backwards on page
  for i  in range(0, len(n1)):
      drawdate = dd.pop()
      day = dd.pop()
      list = [ day+' '+drawdate, n1.pop(), n2.pop(), n3.pop(), n4.pop(), n5.pop(), n6.pop(), bn.pop(), jp.pop()]
      line = '","'.join(list)
      csv = '"%s"' % line
      file.write(csv.encode("utf-8") + os.linesep)

file.close()
print('It is done. Last draw was %s, %s' % (day, drawdate))

