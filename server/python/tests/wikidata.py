## Get a question from wikipedia about a certain author

import random, requests, base64, cStringIO, bleach, re
import wptools, imdb 

from nltk.tokenize import sent_tokenize
from nltk.tokenize import word_tokenize

from PIL import Image
from geotext import GeoText

from dbsettings import *
from pymongo import MongoClient
client = MongoClient()

# get a template
exams = client.test.exams
exam = exams.find_one({'id': 'CS101'})

# some tables we will use
cityTable = meta.tables['urbs']
authorTable = meta.tables['femaleauthors']

cities = [row['city'] for row in con.execute(cityTable.select())]
countries = [row['country'] for row in con.execute(cityTable.select().distinct('country'))]
authors = [row['author'] for row in con.execute(authorTable.select())]

categories = ['People'] # 'People', 'Places', 'Science', 
subcategories = ['Writers'] # 'Musicians','Actors','Politicians','Sports'
subcategoryimage = 'people.jpg'

topics = ['Text','Dates','Places','Books']
topic = random.choice(topics)
question, body = 'Who is this writer?', None
answers = []
answers.append({u'points': 1, u'option': random.choice(authors)})
answers.append({u'points': 0, u'option': random.choice(authors)})
answers.append({u'points': 0, u'option': random.choice(authors)})

author = answers[0]['option'] # in theory all three could be the same
article = wptools.page(author).get_wikidata()

try:
  imageurl = article.images[0]['url'] 
  response = requests.get(imageurl)
  file = cStringIO.StringIO(response.content)
  image = Image.open(file)
  resized = 300, 300
  image.thumbnail(resized, Image.ANTIALIAS)
  buffer = cStringIO.StringIO()
  image.save(buffer, format="JPEG")
  image_str = base64.b64encode(buffer.getvalue())
  imageuri = ("data:" + response.headers['Content-Type'] + ";" + "base64," + image_str)
  body = '<img class="centered" src="%s"/>' % imageuri
except error:
  print 'Could not get image because foo %s' % error
  topic, question = 'Extract', None
  imageuri = subcategoryimage

if topic == 'Books':
  if article.wikidata.has_key('work'):
    books = article.wikidata['work']
    if type(books) is not list : books = [ books ]
    question = 'Who wrote "%s"' % random.choice(books)

if topic == 'Places':
  question, answers = None, []
  html = wptools.page(author).get_query().extract
  text = bleach.clean(html, tags=[], strip=True)
  sentences = sent_tokenize(text)
  for sentence in sentences:
    places = GeoText(sentence).cities
    if len(places) > 0:
      for place in places:
        if place in cities:
          question = 'In which city?'
          body = "%s <br> %s " % (body, sentence.replace(place, '...'))
          answers.append({u'points': 1, u'option': place})
          answers.append({u'points': 0, u'option': random.choice(cities)})
          answers.append({u'points': 0, u'option': random.choice(cities)})
          break
          
  if not question:
    try:
      question = '%s is a citizen of what country' % article.label
      answers = []
      answers.append({u'points': 1, u'option': article.wikidata['citizenship']})
      answers.append({u'points': 0, u'option': random.choice(countries)})
      answers.append({u'points': 0, u'option': random.choice(countries)})
    except:
      question = None
      
if topic == 'Extract':
  html = wptools.page(author).get_query().extract
  text = bleach.clean(html, tags=[], strip=True)
  sentences = sent_tokenize(text)
  body = random.choice(sentences)
  try:
    firstname, lastname = article.label.split(' ')
    body = re.sub(firstname+'.*?'+lastname, 'They', body)
    body = body.replace('They is', 'They are').replace('They was', 'They were')
    question = 'Who are they?'
  except:
    topic = 'Dates'

if topic == 'Dates':
  if article.wikidata.has_key('birth'):
    when = int(article.wikidata['birth'][1:5])
    question = '%s %s was born in which year?' % (article.description, article.label)
  elif article.wikidata.has_key('death'):
    when = int(article.wikidata['death'][1:5])
    question = '%s %s died in which year?' % (article.description, article.label)
  else:
    question = None
    print 'No dates!'
  
  if when:  
    answers = []
    answers.append({u'points': 1, u'option': when })
    answers.append({u'points': 0, u'option': random.choice(range(when-25, when+25)) })
    answers.append({u'points': 0, u'option': random.choice(range(when-25, when+25)) })    

if question:
  newQuestion = exam['questions'][0]
  newQuestion['topic'] = topic
  newQuestion['question'] = body 
  newQuestion['title'] = question
  random.shuffle(answers)  # works in place returns None
  newQuestion['answers'] = answers
  print newQuestion['answers']

  exam['questions'].append(newQuestion)
  exams.update_one({'_id': exam['_id'] }, { '$set' : { 'questions' : exam['questions'] } })
  users = client.test.users
  user = users.find_one({'username':'admin'})
  user['profile']['exams'][3]['questions'].append(newQuestion)
  users.update_one({'_id': user['_id'] }, { '$set' : { 'profile.exams' : user['profile']['exams'] } })
