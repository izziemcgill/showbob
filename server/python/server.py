import requests, json
from flask import Flask, request, Response

app = Flask(__name__)

@app.route('/exam/<article>/', methods = ['GET'])
def get_quiz(article):

    try:
        data_send = json.dumps({
            'question': question
        })
        resp = Response(data_send, status=200, mimetype='application/json')
    except:
        resp = Response("ERROR", status=500, mimetype='application/json')

    resp.headers['Access-Control-Allow-Origin'] = "*"
    return resp

if __name__ == '__main__':
    app.debug = True
    app.run()
